from django.contrib import admin
from hunts.models import Hunt, Mission


class HuntAdmin(admin.ModelAdmin):
    model = Hunt
    list_display = ['title', 'active', 'sortorder',]

class MissionAdmin(admin.ModelAdmin):
    model = Mission


admin.site.register(Mission, MissionAdmin)
admin.site.register(Hunt, HuntAdmin)



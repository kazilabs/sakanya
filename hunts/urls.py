from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from hunts import views

urlpatterns = [
    url(r'^hunts/$', views.HuntList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html'])

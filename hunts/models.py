from __future__ import unicode_literals

from django.db import models
from django.utils.text import slugify
from django.conf import settings


class Hunt(models.Model):
    title = models.CharField(max_length=128, blank=False, null=False)
    slug = models.SlugField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    active = models.BooleanField(default=True)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    logo_image = models.ImageField(upload_to=settings.UPLOAD_DIR, blank=True)
    banner_image = models.ImageField(upload_to=settings.UPLOAD_DIR, blank=True)
    created = models.DateTimeField(editable=False, db_index=True, auto_now_add=True,)
    modified = models.DateTimeField(editable=False, db_index=True, auto_now=True,)
    sortorder = models.PositiveIntegerField(blank=True, default=0, db_index=True,)


    class Meta:
        ordering = ['sortorder']

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Hunt, self).save(*args, **kwargs)

class Mission(models.Model):
    title = models.CharField(max_length=128, blank=False, null=False)
    slug = models.SlugField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    value = models.FloatField(blank=False, default=0.0)
    sortorder = models.PositiveIntegerField(blank=True, default=0, db_index=True,)
    hunt = models.ForeignKey(Hunt, related_name="hunt", on_delete=models.CASCADE)


    class Meta:
        ordering = ['sortorder']

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)

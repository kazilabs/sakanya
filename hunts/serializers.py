from rest_framework import serializers
from hunts.models import Hunt

class HuntSerializer(serializers.ModelSerializer):
    banner_url = serializers.SerializerMethodField()

    class Meta:
        model = Hunt
        fields = ('id', 'title', 'description', 'banner_url')

    def get_banner_url(self, obj):
        return 'http://' + self.context['request'].get_host() + '/media/' + obj.banner_image.url

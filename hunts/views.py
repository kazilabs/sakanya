from hunts.models import Hunt
from hunts.serializers import HuntSerializer
from rest_framework import generics, renderers

class HuntList(generics.ListAPIView):
    """
    List all active hunts
    """
    renderer_classes = [renderers.JSONRenderer]
    queryset = Hunt.objects.filter(active=True)
    serializer_class = HuntSerializer
